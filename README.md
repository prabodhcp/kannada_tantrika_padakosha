<div align="center">
![VTU Logo](/source/images/vtu.png "VTU")
</div>

<!-- blank line -->
----
<!-- blank line -->


<div align="center">

# ಕನ್ನಡ ತಾಂತ್ರಿಕ ಪದಕೋಶ - [ವಿತಾವಿ ಪ್ರಾಯೋಜಿತ ಯೋಜನೆ](https://vtu.ac.in/)

</div>

<!-- blank line -->
<br>
<!-- blank line -->

## ವಿಷಯ - ನೆಟ್ವರ್ಕ್ ಸೆಕ್ಯೂರಿಟಿ ಅಂಡ್ ಸೈಬರ್ ಲಾ (Network Security and Cyber Law)

### _Created & Maintained by_ ಪ್ರಬೋಧ ಸಿ ಪಿ (Prabodh C P)

#### ಜಾಲಬಂಧ ಭದ್ರತೆ ಹಾಗು ಸೈಬರ್ ಕಾನೂನು ವಿಷಯಕ್ಕೆ ಸಂಬಂಧಿಸಿದಂತೆ ಪದಕೋಶ ಮತ್ತು ಪದಾರ್ಥಕೋಶವನ್ನು ಇಲ್ಲಿ ಮಾಡಲಾಗಿದೆ

<!-- blank line -->
----
<!-- blank line -->


To see the current status of **Kannada Technical Dictionary/Glossary** click on 
 [PadaKosha_NetworkSecurityCyberLaw.csv](VTU_FORMAT/Sorted/PadaKosha_NetworkSecurityCyberLaw.csv)

ಈ ಯೋಜನೆಯ ಪ್ರಸಕ್ತ ಪ್ರಗತಿಯನ್ನು ತಿಳಿಯಲು ಇಲ್ಲಿ ಕ್ಲಿಕ್ಕಿಸಿ [PadaKosha_NetworkSecurityCyberLaw.csv](VTU_FORMAT/Sorted/PadaKosha_NetworkSecurityCyberLaw.csv)

